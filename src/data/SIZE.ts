import { DICE } from './DICE'

export const HD_FOR_SIZE = {
  '微型': DICE.d4,
  '小型': DICE.d6,
  '中型': DICE.d8,
  '大型': DICE.d10,
  '巨型': DICE.d12,
  '超巨型': DICE.d20,
}

export const CREATURE_SIZE = [
  '微型',
  '小型',
  '中型',
  '大型',
  '巨型',
  '超巨型',
]
