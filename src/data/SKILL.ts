import { DndStat } from 'src/components/models'

export const SKILL: {
  [key: string]: {
    stat: DndStat
    label: string
  }
} = {
  ACROBATICS: {
    stat: 'DEX',
    label: '體操',
  },
  ANIMAL_HANDLING: {
    stat: 'WIS',
    label: '馴獸',
  },
  ARCANA: {
    stat: 'INT',
    label: '奧秘',
  },
  ATHLETICS: {
    stat: 'STR',
    label: '運動',
  },
  DECEPTION: {
    stat: 'CHA',
    label: '欺瞞',
  },
  HISTORY: {
    stat: 'INT',
    label: '歷史',
  },
  INSIGHT: {
    stat: 'WIS',
    label: '洞悉',
  },
  INTIMIDATION: {
    stat: 'CHA',
    label: '威嚇',
  },
  INVESTIGATION: {
    stat: 'INT',
    label: '調查',
  },
  MEDICINE: {
    stat: 'WIS',
    label: '醫藥',
  },
  NATURE: {
    stat: 'INT',
    label: '自然',
  },
  PERCEPTION: {
    stat: 'WIS',
    label: '察覺',
  },
  PERFORMANCE: {
    stat: 'CHA',
    label: '表演',
  },
  PERSUASION: {
    stat: 'CHA',
    label: '說服',
  },
  RELIGION: {
    stat: 'INT',
    label: '宗教',
  },
  SLIGHT_OF_HAND: {
    stat: 'DEX',
    label: '巧手',
  },
  STEALTH: {
    stat: 'DEX',
    label: '隱匿',
  },
  SURVIVAL: {
    stat: 'WIS',
    label: '生存',
  },
}
